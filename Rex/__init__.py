#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('Rex')
def module():
    transport('Capital.Absent',                     'absent')
    transport('Capital.Codec',                      'encode_ascii')
    transport('Capital.Core',                       'arrange')
    transport('Capital.Core',                       'character')
    transport('Capital.Core',                       'enumerate')
    transport('Capital.Core',                       'Integer')
    transport('Capital.Core',                       'intern_string')
    transport('Capital.Core',                       'is_python_2')
    transport('Capital.Core',                       'length')
    transport('Capital.Core',                       'line')
    transport('Capital.Core',                       'List')
    transport('Capital.Core',                       'none')
    transport('Capital.Core',                       'Object')
    transport('Capital.Core',                       'ordinal')
    transport('Capital.Core',                       'python_debug_mode')
    transport('Capital.Core',                       'String')
    transport('Capital.Core',                       'true')
    transport('Capital.Core',                       'Tuple')
    transport('Capital.Core',                       'type')
    transport('Capital.Exception',                  'raise_value_error')
    transport('Capital.Import',                     'import_module')
    transport('Capital.Map',                        'first_map_item')
    transport('Capital.Map',                        'view_items')
    transport('Capital.PortrayString',              'portray_string')


    share(
        #
        #   Values
        #
        'list_of_single_none',  [none],
    )
