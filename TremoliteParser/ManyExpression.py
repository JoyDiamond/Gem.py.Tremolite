#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('TremoliteParser.ManyExpression')
def module():
    class Tremolite_AndExpression_Many(ManyExpression):
        __slots__    = (())
        display_name = 'tremolite-or-*'


    class Tremolite_OrExpression_Many(ManyExpression):
        __slots__    = (())
        display_name = 'tremolite-or-*'


    [
        conjure_TREMOLITE_and_expression_many, conjure_TREMOLITE_and_expression_many__with_frill,
    ] = produce_conjure_many_expression('tremolite-and-*', Tremolite_AndExpression_Many)

    [
        conjure_TREMOLITE_or_expression_many, conjure_TREMOLITE_or_expression_many__with_frill,
    ] = produce_conjure_many_expression('tremolite-or-*', Tremolite_OrExpression_Many)


    share(
        'conjure_TREMOLITE_and_expression_many',    conjure_TREMOLITE_and_expression_many,
        'conjure_TREMOLITE_or_expression_many',     conjure_TREMOLITE_or_expression_many,
    )
