#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('TremoliteParser.Parse')
def module():
    require_module('TremoliteParser.DualExpressionStatement')
    require_module('TremoliteParser.Elemental')
    require_module('TremoliteParser.Match')
    require_module('TremoliteParser.ParseAtom')
    require_module('TremoliteParser.ParseExpression')
    require_module('TremoliteParser.Statement')


    def parse_TREMOLITE__statement_pattern(m):
        if m.end('newline') != -1:
            raise_unknown_line()

        j = m.end()
        s = qs()

        indentation_end  = m.end('indented')
        name_end         = m.end('name')
        colon_equal_end  = m.end('colon_equal')

        indentation = conjure_indentation          (s[                : indentation_end])
        name        = conjure_name                 (s[indentation_end : name_end])
        colon_equal = conjure_TREMOLITE_colon_equal(s[name_end        : j])

        wi(j)
        wj(j)

        #
        #   <atom>
        #
        atom = parse_TREMOLITE__atom__normal()
        operator = qk()

        if operator is not 0:
            wk0()
        else:
            newline = qn()

            if newline is not 0:
                return conjure_pattern_statement(indentation, name, colon_equal, atom, newline)

            operator = tokenize_TREMOLITE_operator()

        left = parse_TREMOLITE__or_expression__X__any_expression(atom, operator)

        operator = qk()

        if operator is not 0:
            raise_unknown_line()

        newline = qn()

        if newline is 0:
            raise_unknown_line()

        return conjure_pattern_statement(indentation, name, colon_equal, left, newline)


    def parse_TREMOLITE__statement_language(m):
        if m.end('newline') != -1:
            raise_unknown_line()

        j = m.end()
        s = qs()

        indentation_end  = m.end('indented')
        indentation      = conjure_indentation(s[ :  indentation_end])
        keyword_language = conjure_keyword_language(s[indentation_end : j])

        wi(j)
        wj(j)


        #
        #<pattern>
        #
        m = language_pattern_match(s, j)

        if m is none:
            raise_unknown_line()

        pattern_end     = m.end('pattern')
        keyword_pattern = conjure_TREMOLITE_keyword_pattern(s[j            : pattern_end])
        newline         = conjure_line_marker              (s[ pattern_end : ])

        j = m.end()

        wi(j)
        wj(j)
        #</name>

        return conjure_language_pattern_statement(
                conjure_vw_frill(indentation, newline),
                keyword_language,
                keyword_pattern,
            )


    parse_TREMOLITE__lookup_line = {
                                      'language' : parse_TREMOLITE__statement_language,
                                  }.get


    @share
    def parse_TREMOLITE(path, show = 0, test = 0):
        #line("parse_TREMOLITE(path<%s>, show<%d>, test<%d>)", path, show, test);

        data = read_text_from_path(path)

        parse_context = z_initialize(path, data)

        append        = parse_context.append
        data_many     = parse_context.many
        iterate_lines = parse_context.iterate_lines

        for LOOP in parse_context:
            with parse_context:
                for s in iterate_lines:
                    #line('s: %s', s)

                    m = line_match(s)

                    if m is none:
                        raise_unknown_line()

                    keyword_s = m.group('keyword')

                    if keyword_s is not none:
                        parse_line = parse_TREMOLITE__lookup_line(keyword_s)

                        if parse_line is not none:
                            append(parse_line(m))

                            assert qd() is 0
                            continue

                        raise_unknown_line()

                    name_s = m.group('name')

                    if name_s is not none:
                        append(parse_TREMOLITE__statement_pattern(m))

                        assert qd() is 0
                        continue

                    comment_end = m.end('comment')

                    if comment_end is not -1:
                        append(conjure_any_comment_line(m.end('indented'), comment_end))
                        continue

                    if m.end('newline') is -1:
                        raise_unknown_line()

                    append(conjure_empty_line(m.group()))

        data_lines = parse_context.data_lines
        tree_many  = data_many

        if test is 7:
            test_identical_output(path, data, data_many, tree_many)
            test_count_newlines(data_lines, tree_many)

        return data_many
