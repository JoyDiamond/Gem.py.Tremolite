#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('TremoliteParser.ParseExpression')
def module():
    require_module('TremoliteParser.BinaryExpression')
    require_module('TremoliteParser.ManyExpression')
    require_module('TremoliteParser.PostfixExpression')
    require_module('TremoliteParser.TokenizeAtom')


    #
    #   postfix
    #
    @share
    def parse_TREMOLITE__postfix_expression__left_operator(left, operator):
        while 7 is 7:
            assert qk() is 0

            if operator.is_TREMOLITE_postfix_operator:
                left = operator.conjure_postfix_expression(left, operator)

                operator = qk()

                if operator is not 0:
                    if not operator.is_TREMOLITE_postfix_operator:
                        return left

                    wk0()
                else:
                    if qn() is not 0:
                        return left

                    operator = tokenize_TREMOLITE_operator()

                    if not operator.is_TREMOLITE_postfix_operator:
                        wk(operator)

                        return left
            else:
                raise_unknown_line()

            assert operator.is_TREMOLITE_postfix_operator

        raise_unknown_line()


    #
    #   unary
    #
    @share
    def parse_TREMOLITE__unary_expression__operator(operator):
        return operator.conjure_unary_expression(operator, parse_TREMOLITE__unary_expression())


    def parse_TREMOLITE__unary_expression():
        left = parse_TREMOLITE__atom__normal()

        operator = qk()

        if operator is 0:
            if qn() is not 0:
                return left

            operator = tokenize_TREMOLITE_operator()

            if qk() is not 0:
                raise_unknown_line()

            if operator.is_end_of_TREMOLITE_unary_expression:
                wk(operator)
                return left
        else:
            if operator.is_end_of_TREMOLITE_unary_expression:
                return left

            wk0()

        if operator.is_TREMOLITE_postfix_operator:
            left = parse_TREMOLITE__postfix_expression__left_operator(left, operator)

            operator = qk()

            if operator is 0:
                if qn() is 0:
                    raise_unknown_line()

                return left

            if operator.is_end_of_TREMOLITE_unary_expression:
                return left

            wk0()

        my_line('operator: %r', operator)
        raise_unknown_line()


    #
    #   range
    #
    def parse_TREMOLITE__range_expression__left_operator(left, range_operator):
        assert range_operator.is_TREMOLITE_range_operator

        right = parse_TREMOLITE__unary_expression()

        operator = qk()

        if operator is 0:
            if qn() is not 0:
                return conjure_TREMOLITE_range_expression(left, range_operator, right)

            operator = tokenize_TREMOLITE_operator()

            if operator.is_end_of_TREMOLITE_range_expression:
                wk(operator)

                return conjure_TREMOLITE_range_expression(left, range_operator, right)
        else:
            if operator.is_end_of_TREMOLITE_range_expression:
                return conjure_TREMOLITE_range_expression(left, range_operator, right)

            wk0()

        raise_unknown_line()


    def parse_TREMOLITE__range_expression():
        left = parse_TREMOLITE__atom__normal()

        operator = qk()

        if operator is not 0:
            if operator.is_end_of_TREMOLITE_range_expression:
                return left

            wk0()
        else:
            if qn() is not 0:
                return left

            operator = tokenize_TREMOLITE_operator()

            if operator.is_end_of_TREMOLITE_range_expression:
                wk(operator)

                return left

        if operator.is_TREMOLITE_postfix_operator:
            left = parse_TREMOLITE__postfix_expression__left_operator(left, operator)

            operator = qk()

            if operator is 0:
                if qn() is 0:
                    raise_unknown_line()

                return left

            if operator.is_end_of_TREMOLITE_range_expression:
                return left

            wk0()

        if operator.is_TREMOLITE_range_operator:
            return parse_TREMOLITE__range_expression__left_operator(left, operator)

        #my_line('left: %r; operator: %r; s: %s', left, operator, portray_string(qs()[qj():]))
        raise_unknown_line()


    #
    #   add
    #
    parse_TREMOLITE__arithmetic_expression__left_operator = (
            produce_parse_LANGUAGE__X_expression__left_operator(
                'TREMOLITE',                                    #   language
                'arithmetic_expression',                        #   name
                conjure_arithmetic_expression_many,             #   conjure_LANGUAGE_X_expression_many
                conjure_add_expression,                         #   conjure_LANGUAGE_X_expression
                'is_end_of_TREMOLITE_arithmetic_expression',    #   name__is_end_of_X_expression
                'is_plus_sign',                                 #   name__is_X_expression
                parse_TREMOLITE__range_expression,              #   parse_LANGUAGE__W_expression
                tokenize_TREMOLITE_operator,                    #   tokenize_LANGUAGE_operator
            )
        )


    def parse_TREMOLITE__arithmetic_expression():
        left = parse_TREMOLITE__atom__normal()

        operator = qk()

        if operator is not 0:
            if operator.is_end_of_TREMOLITE_arithmetic_expression:
                return left

            wk0()
        else:
            if qn() is not 0:
                return left

            operator = tokenize_TREMOLITE_operator()

            if operator.is_end_of_TREMOLITE_arithmetic_expression:
                wk(operator)

                return left

        if operator.is_TREMOLITE_postfix_operator:
            left = parse_TREMOLITE__postfix_expression__left_operator(left, operator)

            operator = qk()

            if operator is 0:
                if qn() is 0:
                    raise_unknown_line()

                return left

            if operator.is_end_of_TREMOLITE_arithmetic_expression:
                return left

            wk0()

        if operator.is_TREMOLITE_range_operator:
            left = parse_TREMOLITE__range_expression__left_operator(left, operator)

            operator = qk()

            if operator is 0:
                if qn() is 0:
                    raise_unknown_line()

                return left

            if operator.is_end_of_TREMOLITE_arithmetic_expression:
                return left

            wk0()

        if operator.is_plus_sign:
            return parse_TREMOLITE__arithmetic_expression__left_operator(left, operator)

        #my_line('left: %r; operator: %r; s: %s', left, operator, portray_string(qs()[qj():]))
        raise_unknown_line()


    #
    #   and
    #
    parse_TREMOLITE__and_expression__left_operator = (
            produce_parse_LANGUAGE__X_expression__left_operator(
                'TREMOLITE',                                    #   language
                'and_expression',                               #   name
                conjure_TREMOLITE_and_expression_many,          #   conjure_LANGUAGE_X_expression_many
                conjure_TREMOLITE_and_expression,               #   conjure_LANGUAGE_X_expression
                'is_end_of_TREMOLITE_and_expression',           #   name__is_end_of_X_expression
                'is_and_sign',                                  #   name__is_X_expression
                parse_TREMOLITE__arithmetic_expression,         #   parse_LANGUAGE__W_expression
                tokenize_TREMOLITE_operator,                    #   tokenize_LANGUAGE_operator
            )
        )


    @share
    def parse_TREMOLITE__and_expression():
        left = parse_TREMOLITE__atom__normal()

        operator = qk()

        if operator is 0:
            if qn() is not 0:
                return left

            operator = tokenize_TREMOLITE_operator()

            if operator.is_end_of_TREMOLITE_and_expression:
                wk(operator)
                return left
        else:
            if operator.is_end_of_TREMOLITE_and_expression:
                return left

            wk0()

        if operator.is_TREMOLITE_postfix_operator:
            left = parse_TREMOLITE__postfix_expression__left_operator(left, operator)

            operator = qk()

            if operator is 0:
                if qn() is 0:
                    raise_unknown_line()

                return left

            if operator.is_end_of_TREMOLITE_and_expression:
                return left

            wk0()

        if operator.is_TREMOLITE_range_operator:
            left = parse_TREMOLITE__range_expression__left_operator(left, operator)

            operator = qk()

            if operator is 0:
                if qn() is 0:
                    raise_unknown_line()

                return left

            if operator.is_end_of_TREMOLITE_and_expression:
                return left

            wk0()

        if operator.is_plus_sign:
            left = parse_TREMOLITE__arithmetic_expression__left_operator(left, operator)

            operator = qk()

            if operator is 0:
                if qn() is 0:
                    raise_unknown_line()

                return left

            if operator.is_end_of_TREMOLITE_and_expression:
                return left

            wk0()

        if operator.is_and_sign:
            return parse_TREMOLITE__and_expression__left_operator(left, operator)

        #my_line('left: %r; operator: %r; s: %s', left, operator, portray_string(qs()[qj():]))
        raise_unknown_line()


    #
    #   or
    #
    parse_TREMOLITE__or_expression__left_operator = (
            produce_parse_LANGUAGE__X_expression__left_operator(
                'TREMOLITE',                                    #   language
                'or_expression',                                #   name
                conjure_TREMOLITE_or_expression_many,           #   conjure_LANGUAGE_X_expression_many
                conjure_TREMOLITE_or_expression,                #   conjure_LANGUAGE_X_expression
                'is_end_of_TREMOLITE_or_expression',            #   name__is_end_of_X_expression
                'is_or_sign',                                   #   name__is_X_expression
                parse_TREMOLITE__and_expression,                #   parse_LANGUAGE__W_expression
                tokenize_TREMOLITE_operator,                    #   tokenize_LANGUAGE_operator
            )
        )


    @share
    def parse_TREMOLITE__or_expression__X__any_expression(left, operator):
        if operator.is_TREMOLITE_postfix_operator:
            left = parse_TREMOLITE__postfix_expression__left_operator(left, operator)

            operator = qk()

            if operator is 0:
                if qn() is 0:
                    raise_unknown_line()

                return left

            if operator.is_end_of_TREMOLITE_or_expression:
                return left

            wk0()

        if operator.is_TREMOLITE_range_operator:
            left = parse_TREMOLITE__range_expression__left_operator(left, operator)

            operator = qk()

            if operator is 0:
                if qn() is 0:
                    raise_unknown_line()

                return left

            if operator.is_end_of_TREMOLITE_or_expression:
                return left

            wk0()

        if operator.is_plus_sign:
            left = parse_TREMOLITE__arithmetic_expression__left_operator(left, operator)

            operator = qk()

            if operator is 0:
                if qn() is 0:
                    raise_unknown_line()

                return left

            if operator.is_end_of_TREMOLITE_or_expression:
                return left

            wk0()

        if operator.is_and_sign:
            left = parse_TREMOLITE__and_expression__left_operator(left, operator)

            operator = qk()

            if operator is 0:
                if qn() is 0:
                    raise_unknown_line()

                return left

            if operator.is_end_of_TREMOLITE_or_expression:
                return left

            wk0()

        if operator.is_or_sign:
            return parse_TREMOLITE__or_expression__left_operator(left, operator)

        my_line('line: %d; left: %r; operator: %r', ql(), left, operator)
        raise_unknown_line()


    @share
    def parse_TREMOLITE__or_expression():
        left = parse_TREMOLITE__atom__normal()

        operator = qk()

        if operator is 0:
            if qn() is not 0:
                return left

            operator = tokenize_TREMOLITE_operator()

            if operator.is_end_of_TREMOLITE_or_expression:
                wk(operator)
                return left
        else:
            if operator.is_end_of_TREMOLITE_or_expression:
                return left

            wk0()

        return parse_TREMOLITE__or_expression__X__any_expression(left, operator)


    @share
    def parse_TREMOLITE__or_expression__PLUS__operator():
        left = parse_TREMOLITE__atom__normal()

        operator = qk()

        if operator is 0:
            if qn() is not 0:
                raise_unknown_line()

            operator = tokenize_TREMOLITE_operator()

            if operator.is_end_of_TREMOLITE_or_expression:
                wk(operator)
                return left
        else:
            if operator.is_end_of_TREMOLITE_or_expression:
                return left

            wk0()

        r = parse_TREMOLITE__or_expression__X__any_expression(left, operator)

        if qn() is not 0:
            raise_unknown_line()

        assert qk() is not 0

        return r
