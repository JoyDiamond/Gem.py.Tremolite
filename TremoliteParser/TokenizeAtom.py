#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('TremoliteParser.TokenizeAtom')
def module():
    require_module('TremoliteParser.DualToken')


    TREMOLITE__skip_tokenize_prefix = produce__LANGUAGE__skip_tokenize_prefix('tremolite', next_crystal_nested_line_match)


    #
    #   analyze_LANGUAGE_newline_* (without `newline` in the name)
    #
    [
            analyze_TREMOLITE_comma_operator,                           #   0
            analyze_TREMOLITE_keyword_atom,
            analyze_TREMOLITE_operator,
            analyze_TREMOLITE_quote,
    ] = produce_analyze_LANGUAGE_functions(
            'tremolite',
            true,                                                       #   has_open_operator = true
            0,
            find_TREMOLITE_atom_type,
            lookup_TREMOLITE_keyword_conjure_function,
            TREMOLITE__skip_tokenize_prefix,
        )


    assert analyze_TREMOLITE_comma_operator is 0


    @share
    def parse_TREMOLITE_name():
        assert qk() is 0

        if qn() is not 0:
            raise_unknown_line()

        m = TREMOLITE_name_match(qs(), qj())

        if m is none:
            raise_unknown_line()

        return analyze_TREMOLITE_keyword_atom(m, m.group('atom'))


    @share
    def analyze_TREMOLITE_atom(m):
        atom_s = m.group('atom')

        if atom_s is not none:
            return analyze_TREMOLITE_keyword_atom(m, atom_s)

        operator_s = m.group('operator')

        if operator_s is not none:
            return analyze_TREMOLITE_operator(m, operator_s)

        quote_start = m.start('quote')

        if quote_start is not -1:
            return analyze_TREMOLITE_quote(m, quote_start)

        raise_unknown_line()


    @share
    def tokenize_TREMOLITE_colon():
        assert qk() is 0

        if qn() is not 0:
            raise_unknown_line()

        m = TREMOLITE_colon_match(qs(), qj())

        if m is none:
            raise_unknown_line()

        return analyze_TREMOLITE_operator(m, m.group('operator'))


    @share
    def tokenize_TREMOLITE_operator():
        assert qk() is 0

        if qn() is not 0:
            raise_unknown_line()

        s = qs()

        #my_line('d: %d; full: %r; s: %r', qd(), s, portray_string(s[qj() : ]))

        m = TREMOLITE_operator_match(s, qj())

        if m is none:
            #my_line(portray_string(s[qj() : ]))
            raise_unknown_line()

        operator_s = m.group('operator')

        if operator_s is not none:
            return analyze_TREMOLITE_operator(m, operator_s)

        raise_unknown_line()
