#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('TremoliteParser.BookcaseManyExpression')
def module():
    class Tremolite_ComplementSetExpression_Many(BookcaseManyExpression):
        __slots__    = (())
        display_name = '{~*~}'

        #<atom>
        is_CRYSTAL_atom = true
        #</atom>


    class Tremolite_Group_ComplementSetExpression_Many(BookcaseManyExpression):
        __slots__    = (())
        display_name = '{~group:*~}'

        #<atom>
        is_CRYSTAL_atom = true
        #</atom>


    class Tremolite_Group_SetExpression_Many(BookcaseManyExpression):
        __slots__    = (())
        display_name = '{|group:*|}'

        #<atom>
        is_CRYSTAL_atom = true
        #</atom>


    class Tremolite_SetExpression_Many(BookcaseManyExpression):
        __slots__    = (())
        display_name = '{|*|}'

        #<atom>
        is_CRYSTAL_atom = true
        #</atom>


    #
    #   NOTE:
    #       `conjure_TREMOLITE_*__with_frill` is not used yet, but will be used in the future.
    #
    [
        conjure_TREMOLITE_complement_set_expression_many,
        conjure_TREMOLITE_complement_set_expression_many__with_frill,
    ] = produce_conjure_bookcase_many_expression(
            'tremolite-complement-set-expression-*',
            Tremolite_ComplementSetExpression_Many,
        )


    [
        conjure_TREMOLITE_group_complement_set_expression_many,
        conjure_TREMOLITE_group_complement_set_expression_many__with_frill,
    ] = produce_conjure_bookcase_many_expression(
            'tremolite-group-complement-set-expression-*',
            Tremolite_Group_ComplementSetExpression_Many,
        )


    [
        conjure_TREMOLITE_group_set_expression_many, conjure_TREMOLITE_group_set_expression_many__with_frill,
    ] = produce_conjure_bookcase_many_expression(
            'tremolite-group-set-expression-*',
            Tremolite_Group_SetExpression_Many,
        )


    [
        conjure_TREMOLITE_set_expression_many, conjure_TREMOLITE_set_expression_many__with_frill,
    ] = produce_conjure_bookcase_many_expression('tremolite-set-expression-*', Tremolite_SetExpression_Many)


    share(
        'conjure_TREMOLITE_complement_set_expression_many',     conjure_TREMOLITE_complement_set_expression_many,

        'conjure_TREMOLITE_group_complement_set_expression_many',
            conjure_TREMOLITE_group_complement_set_expression_many,

        'conjure_TREMOLITE_group_set_expression_many',  conjure_TREMOLITE_group_set_expression_many,
        'conjure_TREMOLITE_set_expression_many',        conjure_TREMOLITE_set_expression_many,
    )
