#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('TremoliteParser.ParseAtom')
def module():
    require_module('TremoliteParser.ParseSet')
    require_module('TremoliteParser.UnaryExpression')


    def parse_TREMOLITE__group_at_expression__at_sign(at_sign):
        name = parse_TREMOLITE_name()

        return conjure_TREMOLITE_group_at_expression(at_sign, name)


    def parse_TREMOLITE__brace_expression__left_brace(left_brace):
        middle_1 = parse_TREMOLITE__or_expression__PLUS__operator()

        operator_1 = qk()
        wk0()

        if (middle_1.is_CRYSTAL_identifier) and (operator_1.is_colon):
            middle_2 = parse_TREMOLITE__or_expression__PLUS__operator()

            operator_2 = qk()
            wk0()

            if not operator_2.is_right_brace:
                raise_unknown_line()

            return conjure_TREMOLITE_group_brace_expression(
                    left_brace,
                    middle_1,
                    operator_1,
                    middle_2,
                    operator_2,
                )

        if not operator_1.is_right_brace:
            #my_line('operator_1: %s', operator_1)
            raise_unknown_line()

        return conjure_TREMOLITE_anonymous_group_brace_expression(left_brace, middle_1, operator_1)


    def parse_TREMOLITE__optional_expression__left_square_bracket(left_square_bracket):
        middle_1 = parse_TREMOLITE__or_expression__PLUS__operator()

        operator_1 = qk()
        wk0()

        if (middle_1.is_CRYSTAL_identifier) and (operator_1.is_colon):
            middle_2 = parse_TREMOLITE__or_expression__PLUS__operator()

            operator_2 = qk()
            wk0()

            if not operator_2.is_right_square_bracket:
                raise_unknown_line()

            return conjure_TREMOLITE_group_optional_expression(
                    left_square_bracket,
                    middle_1,
                    operator_1,
                    middle_2,
                    operator_2,
                )

        if not operator_1.is_right_square_bracket:
            raise_unknown_line()

        return conjure_TREMOLITE_optional_expression(left_square_bracket, middle_1, operator_1)


    def parse_TREMOLITE__parenthesized_expression__left_parenthesis(left_parenthesis):
        middle_1 = parse_TREMOLITE__or_expression__PLUS__operator()

        operator_1 = qk()
        wk0()

        if not operator_1.is_right_parenthesis:
            raise_unknown_line()

        return conjure_CRYSTAL_parenthesized_expression(left_parenthesis, middle_1, operator_1)


    @share
    def parse_TREMOLITE__atom__X__token(token):
        if token.is_at_sign:
            return parse_TREMOLITE__group_at_expression__at_sign(token)

        if token.is_left_parenthesis:
            return parse_TREMOLITE__parenthesized_expression__left_parenthesis(token)

        if token.is_TREMOLITE__left_brace__set:
            return parse_TREMOLITE___set_expression___left_brace__set(token)

        if token.is_TREMOLITE__left_brace__tilde:
            return parse_TREMOLITE___complement_set_expression___left_brace__tilde(token)

        if token.is_left_square_bracket:
            return parse_TREMOLITE__optional_expression__left_square_bracket(token)

        if token.is_left_brace:
            return parse_TREMOLITE__brace_expression__left_brace(token)

        if token.is_TREMOLITE_unary_operator:
            return parse_TREMOLITE__unary_expression__operator(token)

        #my_line('token: %r', token)
        raise_unknown_line()


    @share
    def parse_TREMOLITE__atom__normal():
        assert qk() is 0

        if qn() is not 0:
            assert 0
            raise_unknown_line()

        m = TREMOLITE_atom_match(qs(), qj())

        if m is none:
            my_line('full: %r; s: %r', portray_string(qs()), portray_string(qs()[qj() :]))
            raise_unknown_line()

        token = analyze_TREMOLITE_atom(m)

        if token.is_CRYSTAL_atom:
            return token

        return parse_TREMOLITE__atom__X__token(token)
