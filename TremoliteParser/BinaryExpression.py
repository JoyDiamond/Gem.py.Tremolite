#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('TremoliteParser.BinaryExpression')
def module():
    @export
    class Tremolite_AndExpression(BinaryExpression):
        __slots__    = (())
        display_name = 'tremolite-and-expression'
        frill        = W__AND_SIGN__W


    @export
    class Tremolite_OrExpression(BinaryExpression):
        __slots__    = (())
        display_name = 'tremolite-or-expression'
        frill        = W__OR_SIGN__W


    @export
    class Tremolite_RangeExpression(BinaryExpression):
        __slots__    = (())
        display_name = 'range-expression'
        frill        = conjure_action_word('..', '..')


    #
    #   conjure
    #
    conjure_TREMOLITE_and_expression = produce_conjure_binary_expression(
            'tremolite-and-expression',
            Tremolite_AndExpression,
        )

    conjure_TREMOLITE_or_expression = produce_conjure_binary_expression(
            'tremolite-or-expression',
            Tremolite_OrExpression,
        )

    conjure_TREMOLITE_range_expression = produce_conjure_binary_expression(
            'range-expression',
            Tremolite_RangeExpression,
        )


    #
    #   export
    #
    export(
        'conjure_TREMOLITE_and_expression',     conjure_TREMOLITE_and_expression,
        'conjure_TREMOLITE_or_expression',      conjure_TREMOLITE_or_expression,
        'conjure_TREMOLITE_range_expression',   conjure_TREMOLITE_range_expression,
    )
