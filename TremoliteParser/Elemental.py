#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('TremoliteParser.Elemental')
def module():
    class Tremomlite_KeywordPattern(KeywordAndOperatorBase):
        __slots__    = (())
        display_name = 'pattern'


    @share
    class Tremolite_Operator_NotWordBoundary(KeywordAndOperatorBase):
        __slots__    = (())
        display_name = '!/'

        is_TREMOLITE_postfix_operator = true
        is_TREMOLITE_unary_operator   = true


    class Tremolite_OperatorRange(KeywordAndOperatorBase):
        __slots__    = (())
        display_name = '..'

        is_end_of_TREMOLITE_unary_expression = true
        is_TREMOLITE_range_operator          = true


    class Tremolite_OperatorColonEqual(KeywordAndOperatorBase):
        __slots__    = (())
        display_name = ':'


    class Tremolite_Operator_LeftBrace_Set(KeywordAndOperatorBase):
        __slots__    = (())
        display_name = '{|'                                             #   }

        is_TREMOLITE__left_brace__set = true


        display_token = display_token__with_angle_signs
        dump_token    = dump_token__with_angle_signs


    class Tremolite_Operator_LeftBrace_Tilde(KeywordAndOperatorBase):
        __slots__    = (())
        display_name = '{~'                                             #   }

        is_TREMOLITE__left_brace__tilde = true


        display_token = display_token__with_angle_signs
        dump_token    = dump_token__with_angle_signs


    class Tremolite_Operator_Set_RightBrace(KeywordAndOperatorBase):
        __slots__ = (())

        #  {
        display_name = '|}'

        is_TREMOLITE___simple_atom___or___set__right_brace = true

        is_end_of_TREMOLITE_and_expression        = true
        is_end_of_TREMOLITE_arithmetic_expression = true
        is_end_of_TREMOLITE_or_expression         = true
        is_end_of_TREMOLITE_range_expression      = true
        is_end_of_TREMOLITE_unary_expression      = true
        is_TREMOLITE__set__right_brace            = true


        display_token = display_token__with_angle_signs
        dump_token    = dump_token__with_angle_signs


    class Tremolite_Operator_Tilde_RightBrace(KeywordAndOperatorBase):
        __slots__ = (())

        #  {
        display_name = '~}'

        is_TREMOLITE___simple_atom___or___tilde__right_brace = true

        is_end_of_TREMOLITE_and_expression        = true
        is_end_of_TREMOLITE_arithmetic_expression = true
        is_end_of_TREMOLITE_or_expression         = true
        is_end_of_TREMOLITE_range_expression      = true
        is_end_of_TREMOLITE_unary_expression      = true
        is_TREMOLITE__tilde__right_brace          = true


        display_token = display_token__with_angle_signs
        dump_token    = dump_token__with_angle_signs


    initialize_action_word__Meta(
        ((
             ((     '!/',       Tremolite_Operator_NotWordBoundary      )),
             ((     '..',       Tremolite_OperatorRange                 )),
             ((     ':=',       Tremolite_OperatorColonEqual            )),
             ((     '{|',       Tremolite_Operator_LeftBrace_Set        )),
             ((     '|}',       Tremolite_Operator_Set_RightBrace       )),
             ((     '{~',       Tremolite_Operator_LeftBrace_Tilde      )),
             ((     '~}',       Tremolite_Operator_Tilde_RightBrace     )),
        )),
    )


    del Shared.initialize_action_word__Meta


#   [
#       conjure_TREMOLITE__at_colon, conjure_TREMOLITE__at_colon__ends_in_newline,
#   ] = produce_conjure_action_word__ends_in_newline('at-colon', Tremolite_AtColon)


    conjure_TREMOLITE_colon_equal = produce_conjure_action_word__normal('colon-equal', Tremolite_OperatorColonEqual)

#   conjure_TREMOLITE_operator_range = produce_conjure_action_word__normal('operator-range', Tremolite_OperatorRange)

    conjure_TREMOLITE_keyword_pattern = produce_conjure_action_word__normal(
            'keyword-pattern',
            Tremomlite_KeywordPattern,
        )

#   conjure_TREMOLITE__left_brace__set = produce_conjure_action_word__normal(
#           'left-brace--set',
#           Tremolite_Operator_LeftBrace_Set,
#       )

    [
        conjure_TREMOLITE__set__right_brace, conjure_TREMOLITE__set__right_brace__ends_in_newline,
    ] = produce_conjure_action_word__ends_in_newline('right-brace--set', Tremolite_Operator_Set_RightBrace)


    #
    #   Tokens
    #
    AT_SIGN__W           = conjure_action_word          ('@', '@ ')
    LEFT_BRACE__SET__W   = conjure_action_word          ('{|', '{| ')   #   }}
    LEFT_BRACE__TILDE__W = conjure_action_word          ('{~', '{~ ')   #   }}
    NOT_WORD_BOUNDARY    = conjure_action_word          ('!/', '!/')
    W__COLON_EQUAL__W    = conjure_TREMOLITE_colon_equal(' := ')
    W__NOT_SOLIDUS       = conjure_action_word          ('!/', ' !/')

    #{{
    W__SET__RIGHT_BRACE = conjure_action_word('|}', ' |}')

    #{{
    W__TILDE__RIGHT_BRACE = conjure_action_word('~}', ' ~}')



    #
    #   find_TREMOLITE_atom_type
    #
    find_TREMOLITE_atom_type = {
            '"' : conjure_double_quote,
            "'" : conjure_single_quote,

            'A' : conjure_name, 'B' : conjure_name, 'C' : conjure_name, 'D' : conjure_name, 'E' : conjure_name,
            'F' : conjure_name, 'G' : conjure_name, 'H' : conjure_name, 'I' : conjure_name, 'J' : conjure_name,
            'K' : conjure_name, 'L' : conjure_name, 'M' : conjure_name, 'N' : conjure_name, 'O' : conjure_name,
            'P' : conjure_name, 'Q' : conjure_name, 'R' : conjure_name, 'S' : conjure_name, 'T' : conjure_name,
            'U' : conjure_name, 'V' : conjure_name, 'W' : conjure_name, 'X' : conjure_name, 'Y' : conjure_name,
            'Z' : conjure_name, '_' : conjure_name,

            'a' : conjure_name, 'b' : conjure_name, 'c' : conjure_name, 'd' : conjure_name, 'e' : conjure_name,
            'f' : conjure_name, 'g' : conjure_name, 'h' : conjure_name, 'i' : conjure_name, 'j' : conjure_name,
            'k' : conjure_name, 'l' : conjure_name, 'm' : conjure_name, 'n' : conjure_name, 'o' : conjure_name,
            'p' : conjure_name, 'q' : conjure_name, 'r' : conjure_name, 's' : conjure_name, 't' : conjure_name,
            'u' : conjure_name, 'v' : conjure_name, 'w' : conjure_name, 'x' : conjure_name, 'y' : conjure_name,
            'z' : conjure_name,
        }.__getitem__


    #
    #   lookup_TREMOLITE_keyword_conjure_function
    #
    lookup_TREMOLITE_keyword_conjure_function = {
        }.get


    #
    #   share
    #
    share(
        'AT_SIGN__W',                                       AT_SIGN__W,
#       'conjure_TREMOLITE__at_colon',                      conjure_TREMOLITE__at_colon,
#       'conjure_TREMOLITE__at_colon__ends_in_newline',     conjure_TREMOLITE__at_colon__ends_in_newline,
        'conjure_TREMOLITE_colon_equal',                    conjure_TREMOLITE_colon_equal,
        'conjure_TREMOLITE_keyword_pattern',                conjure_TREMOLITE_keyword_pattern,
#       'conjure_TREMOLITE__left_brace__set',               conjure_TREMOLITE__left_brace__set,
#       'conjure_TREMOLITE_operator_range',                 conjure_TREMOLITE_operator_range,
        'conjure_TREMOLITE__set__right_brace',              conjure_TREMOLITE__set__right_brace,

        'conjure_TREMOLITE__set__right_brace__ends_in_newline',
            conjure_TREMOLITE__set__right_brace__ends_in_newline,

        'find_TREMOLITE_atom_type',                     find_TREMOLITE_atom_type,
        'LEFT_BRACE__TILDE__W',                         LEFT_BRACE__TILDE__W,
        'LEFT_BRACE__SET__W',                           LEFT_BRACE__SET__W,
        'lookup_TREMOLITE_keyword_conjure_function',    lookup_TREMOLITE_keyword_conjure_function,
        'W__COLON_EQUAL__W',                            W__COLON_EQUAL__W,
        'W__SET__RIGHT_BRACE',                          W__SET__RIGHT_BRACE,
        'W__TILDE__RIGHT_BRACE',                        W__TILDE__RIGHT_BRACE,
        'NOT_WORD_BOUNDARY',                            NOT_WORD_BOUNDARY,
        'W__NOT_SOLIDUS',                               W__NOT_SOLIDUS,

    )
