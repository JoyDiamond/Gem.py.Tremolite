#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('TremoliteParser.DualToken')
def module():
    class Tremolite_Comma_Complement_RightBrace(DualToken):
        __slots__ = (())

        #   {
        display_name = ',~}'

        is_end_of_TREMOLITE_and_expression        = true
        is_end_of_TREMOLITE_arithmetic_expression = true
        is_end_of_TREMOLITE_or_expression         = true
        is_end_of_TREMOLITE_range_expression      = true
        is_end_of_TREMOLITE_unary_expression      = true


        display_token = display_token__with_angle_signs
        dump_token    = dump_token__with_angle_signs


    class Tremolite_Comma_Set_RightBrace(DualToken):
        __slots__ = (())

        #   {
        display_name = ',|}'

        is_end_of_TREMOLITE_and_expression        = true
        is_end_of_TREMOLITE_arithmetic_expression = true
        is_end_of_TREMOLITE_or_expression         = true
        is_end_of_TREMOLITE_range_expression      = true
        is_end_of_TREMOLITE_unary_expression      = true


        display_token = display_token__with_angle_signs
        dump_token    = dump_token__with_angle_signs


    #
    #   conjure
    #
    conjure_TREMOLITE__comma__complement__right_brace = produce_conjure_dual_token__normal(
            'comma__complement__right_brace',
            Tremolite_Comma_Complement_RightBrace,
        )

    conjure_TREMOLITE__comma__set__right_brace = produce_conjure_dual_token__normal(
            'comma__set__right_brace',
            Tremolite_Comma_Set_RightBrace,
        )


    #
    #   Share
    #
    share(
        'conjure_TREMOLITE__comma__complement__right_brace',    conjure_TREMOLITE__comma__complement__right_brace,
        'conjure_TREMOLITE__comma__set__right_brace',           conjure_TREMOLITE__comma__set__right_brace,
    )
