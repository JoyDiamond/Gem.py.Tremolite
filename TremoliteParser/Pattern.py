#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('TremoliteParser.Pattern')
def module():
    require_module('Restructure.Build')
    require_module('Restructure.CreateMatch')
    require_module('Restructure.Name')


    from Restructure import create_match_code, ANY_OF, BACKSLASH, DOT, EMPTY, END_OF_PATTERN, EXACT
    from Restructure import G, LINEFEED, MATCH, NAME, NAMED_GROUP, NOT_ANY_OF, NOT_FOLLOWED_BY
    from Restructure import ONE_OR_MORE, OPTIONAL, PRINTABLE, PRINTABLE_MINUS, Q, ZERO_OR_MORE


    FULL_MATCH = MATCH
    P          = OPTIONAL


    @share
    def create__TREMOLITE_parser__match():
        alphanumeric_or_underscore = NAME('alphanumeric_or_underscore', ANY_OF('0-9', 'A-Z', '_', 'a-z'))
        letter_or_underscore       = NAME('letter_or_underscore',       ANY_OF('A-Z', '_', 'a-z'))
        ow                         = NAME('ow',                         ZERO_OR_MORE(' '))
        w                          = NAME('w',                          ONE_OR_MORE(' '))

        #
        #   Simple patterns
        #
        and_sign            = NAME('and_sign',            '&')
        at_sign             = NAME('at_sign',             '@')
        colon_equal         = NAME('colon_equal',         ':=')
        colon               = NAME('colon',               ':')
        comma               = NAME('comma',               ',')
        greater_than_sign   = NAME('greater_than_sign',   '>')
        keyword_language    = NAME('language',            'language')
        keyword_pattern     = NAME('pattern',             'pattern')
        left_brace          = NAME('left_brace',          '{')          #   }
        left_brace__tilde   = NAME('left_brace__tilde',   '{~')         #   }
        left_parenthesis    = NAME('left_parenthesis',    '(')          #   )
        left_square_bracket = NAME('left_square_bracket', '[')          #   ]
        less_than_sign      = NAME('less_than_sign',      '<')
        not_word_boundary   = NAME('not_word_boundary',   '!/')
        operator_range      = NAME('operator_range',      '..')
        or_sign             = NAME('or_sign',             '|')
        plus_sign           = NAME('plus_sign',           '+')
        solidus             = NAME('solidus',             '/')
        star                = NAME('star',                '*')
        tilde               = NAME('tilde',               '~')

        name                = NAME('name',   letter_or_underscore + ZERO_OR_MORE(alphanumeric_or_underscore))
        number              = NAME('number', '0' | ANY_OF('1-9') + ZERO_OR_MORE(ANY_OF('0-9')))
        period              = NAME('period', '.')


        double_quote = NAME('double_quote', '"' + ONE_OR_MORE(PRINTABLE_MINUS('"', '\\')) + '"')
        single_quote = NAME('single_quote', "'" + ONE_OR_MORE(PRINTABLE_MINUS("'", '\\')) + "'")


        #   [({{
        right_brace          = NAME('right_brace',          '}')
        right_parenthesis    = NAME('right_parenthesis',    ')')
        right_square_bracket = NAME('right_square_bracket', ']')
#       set__right_brace     = NAME('set__right_brace',     '|}')
        tilde__right_brace   = NAME('tilde__right_brace',   '~}')


        #
        #   More complicated patterns
        #
        comment_newline      = NAME('comment_newline',      P('#' + ZERO_OR_MORE(DOT)) + LINEFEED + END_OF_PATTERN)
        left_parenthesis__ow = NAME('left_parenthesis__ow', left_parenthesis + ow)


        #
        #   Generic
        #
        next_crystal_nested_line_match = MATCH('next_crystal_nested_line_match', ow + Q('newline', comment_newline))


        #
        #   Line
        #
        MATCH(
            'line_match',
            (
                  G('indented', ow)
                + P(
                        G('keyword', keyword_language) + ow
                      | G(name) + ow + G(colon_equal) + ow
                  )
                + Q(
                    'newline',
                    Q('comment', '#' + ZERO_OR_MORE(DOT)) + LINEFEED + END_OF_PATTERN
                  )
            ),
        )


        #
        #   Statements
        #
        MATCH(
            'language_pattern_match',
            (
                  ow
                + G('pattern', keyword_pattern)
                + P(comment_newline)
            ),
        )


        #
        #   Expressions
        #
        MATCH(
            'TREMOLITE_atom_match',
            (
                  G('quote', double_quote | single_quote) + ow          #   Must preceed 'name'
                | G('atom', name) + ow
                | G(
                      'operator',
                      (
                            left_brace + OPTIONAL(ANY_OF('~', '|'))
                          | not_word_boundary
                          | ANY_OF(at_sign, left_parenthesis, left_square_bracket, solidus, tilde)
                      )
                  ) + ow
            ) + Q('newline', comment_newline),
        )

        MATCH(
            'TREMOLITE_name_match',
            G('atom', name) + ow + Q('newline', comment_newline),
        )

        MATCH(
           'TREMOLITE_operator_match',
            (
                  G(
                      'operator',
                       (
                               greater_than_sign + OPTIONAL(greater_than_sign)
                             | not_word_boundary
                             | operator_range
                             | or_sign + OPTIONAL(right_brace)
                             | tilde__right_brace
                             | ANY_OF(
                                   and_sign, colon, comma, plus_sign, solidus, right_brace, right_parenthesis,
                                   right_square_bracket,
                               )
                       ),
                  ) + ow
            ) + Q('newline', comment_newline),
        )

        MATCH(
            'TREMOLITE_colon_match',
            G('operator', colon) + ow + Q('newline', comment_newline),
        )


        #
        #   Create .../TremoliteParser/Match.py
        #
        create_match_code(
                path_join(module_path[0], 'TremoliteParser/Match.py'),
                '2017-2018',
                'Joy Diamond',
                'TremoliteParser.Match',
            )
