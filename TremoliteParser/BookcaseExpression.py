#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('TremoliteParser.BookcaseExpression')
def module():
    class Tremolite_Anonymous_Group_BraceExpression(BookcaseExpression):
        __slots__      = (())
        display_name   = '{}'
        frill          = LEFT_BRACE__RIGHT_BRACE

        #<atom>
        is_CRYSTAL_atom = true
        #</atom>


    class TremoliteComplementSetExpression_1(BookcaseExpression):
        __slots__      = (())
        display_name   = '{~1~}'
        frill          = conjure_vw_frill(LEFT_BRACE__TILDE__W, W__TILDE__RIGHT_BRACE)

        #<atom>
        is_CRYSTAL_atom = true
        #</atom>


    class TremoliteOptionalExpression(BookcaseExpression):
        __slots__    = (())
        display_name = '[]'
        frill        = LEFT_SQUARE_BRACKET__RIGHT_SQUARE_BRACKET

        #<atom>
        is_CRYSTAL_atom = true
        #</atom>


    class TremoliteSetExpression_1(BookcaseExpression):
        __slots__      = (())
        display_name   = '{|1|}'
        frill          = conjure_vw_frill(LEFT_BRACE__SET__W, W__SET__RIGHT_BRACE)

        #<atom>
        is_CRYSTAL_atom = true
        #</atom>


    #
    #   NOTE:
    #       `conjure_TREMOLITE_*__with_frill` not yet used, but will be used in future
    #
    [
        conjure_TREMOLITE_anonymous_group_brace_expression,
        conjure_TREMOLITE_anonymous_group_brace_expression__with_frill,
    ] = produce_conjure_bookcase_expression(
            'anonymous-group-brace-expression',
            Tremolite_Anonymous_Group_BraceExpression,
        )

    [
        conjure_TREMOLITE_optional_expression, conjure_TREMOLITE_optional_expression__with_frill,
    ] = produce_conjure_bookcase_expression('parenthesized-expression', TremoliteOptionalExpression)


    [
        conjure_TREMOLITE_complement_set_expression_1, conjure_TREMOLITE_complement_set_expression_1__with_frill,
    ] = produce_conjure_bookcase_expression('complement-set-expression-1', TremoliteComplementSetExpression_1)


    [
        conjure_TREMOLITE_set_expression_1, conjure_TREMOLITE_set_expression_1__with_frill,
    ] = produce_conjure_bookcase_expression('set-expression-1', TremoliteSetExpression_1)


    share(
        'conjure_TREMOLITE_anonymous_group_brace_expression',   conjure_TREMOLITE_anonymous_group_brace_expression,
        'conjure_TREMOLITE_complement_set_expression_1',        conjure_TREMOLITE_complement_set_expression_1,
        'conjure_TREMOLITE_optional_expression',                conjure_TREMOLITE_optional_expression,
        'conjure_TREMOLITE_set_expression_1',                   conjure_TREMOLITE_set_expression_1,
    )
