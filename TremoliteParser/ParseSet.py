#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('TremoliteParser.ParseSet')
def module():
    require_module('TremoliteParser.BookcaseDualExpression')
    require_module('TremoliteParser.BookcaseExpression')
    require_module('TremoliteParser.BookcaseManyExpression')
    require_module('TremoliteParser.DualToken')


    def produce_parse_LANGUAGE___SET_EXPRESSION___LEFT_OPERATOR__SET(
            language,
            name,
            LEFT_OPERATOR__SET,
            SET__RIGHT_OPERATOR,
            conjure_LANGUAGE__comma__SET__RIGHT_OPERATOR,
            conjure_LANGUAGE_group_SET_EXPRESSION_1,
            conjure_LANGUAGE_group_SET_EXPRESSION_MANY,
            conjure_LANGUAGE_SET_EXPRESSION_1,
            conjure_LANGUAGE_SET_EXPRESSION_MANY,
            name__is_LANGUAGE__SET__RIGHT_OPERATOR,
            name__is_LANGUAGE___simple_atom___or___SET__RIGHT_OPERATOR,
    ):
        @rename('parse_%s___middle_%s___or___%s', language, name, SET__RIGHT_OPERATOR)
        def parse_LANGUAGE___middle_SET_EXPRESSION___or___SET__RIGHT_OPERATOR():
            assert qk() is 0

            if qn() is not 0:
                raise_unknown_line()

            #
            #<LANGUAGE atom or right-brace set + operator>
            #
            m = TREMOLITE_atom_match(qs(), qj())

            if m is none:
                raise_unknown_line()

            atom_or_token = analyze_TREMOLITE_atom(m)

            if attribute(atom_or_token, name__is_LANGUAGE___simple_atom___or___SET__RIGHT_OPERATOR):
                if attribute(atom_or_token, name__is_LANGUAGE__SET__RIGHT_OPERATOR):
                    return atom_or_token

                operator = tokenize_TREMOLITE_operator()

                if operator.is_end_of_TREMOLITE_or_expression:
                    wk(operator)
                    return atom_or_token
            else:
                atom_or_token = parse_TREMOLITE__atom__X__token(atom_or_token)

                operator = qk()

                if operator is 0:
                    operator = tokenize_TREMOLITE_operator()

                    if operator.is_end_of_TREMOLITE_or_expression:
                        wk(operator)
                        return atom_or_token
                else:
                    if operator.is_end_of_TREMOLITE_or_expression:
                        return atom_or_token

                    wk0()
            #</>

            return parse_TREMOLITE__or_expression__X__any_expression(atom_or_token, operator)


        @rename('parse_%s___group_%s___or___%s__group__colon', language, name, LEFT_OPERATOR__SET)
        def parse_LANGUAGE___group_SET_EXPRESSION___LEFT_OPERATOR__SET___group___colon(left_operator__set, group, colon):
            #
            #   1
            #
            middle_1 = parse_TREMOLITE__or_expression__PLUS__operator()

            operator_1 = qk()
            wk0()

            if attribute(operator_1, name__is_LANGUAGE__SET__RIGHT_OPERATOR):
                return conjure_LANGUAGE_group_SET_EXPRESSION_1(left_operator__set, group, colon, middle_1, operator_1)

            if not operator_1.is_comma:
                raise_unknown_line()

            #
            #   2
            #
            middle_2 = parse_LANGUAGE___middle_SET_EXPRESSION___or___SET__RIGHT_OPERATOR()

            if attribute(middle_2, name__is_LANGUAGE__SET__RIGHT_OPERATOR):
                return conjure_LANGUAGE_group_SET_EXPRESSION_1(
                           left_operator__set,
                           group,
                           colon,
                           middle_1,
                           conjure_LANGUAGE__comma__SET__RIGHT_OPERATOR(operator_1, middle_2),
                       )

            #
            #   many
            #
            return parse_LANGUAGE__group_SET_EXPRESSION__many(
                    left_operator__set,
                    [group, middle_1, middle_2],
                    [colon, operator_1],
                )


        @rename('parse_%s___%s___%s', language, name, LEFT_OPERATOR__SET)
        def parse_LANGUAGE___SET_EXPRESSION___LEFT_OPERATOR__SET(left_operator__set):
            #
            #   1
            #
            middle_1 = parse_TREMOLITE__or_expression__PLUS__operator()

            operator_1 = qk()
            wk0()

            if (middle_1.is_CRYSTAL_identifier) and (operator_1.is_colon):
                return parse_LANGUAGE___group_SET_EXPRESSION___LEFT_OPERATOR__SET___group___colon(
                        left_operator__set,
                        middle_1,
                        operator_1,
                    )

            if attribute(operator_1, name__is_LANGUAGE__SET__RIGHT_OPERATOR):
                return conjure_LANGUAGE_SET_EXPRESSION_1(left_operator__set, middle_1, operator_1)

            if not operator_1.is_comma:
                raise_unknown_line()

            #
            #   2
            #
            middle_2 = parse_LANGUAGE___middle_SET_EXPRESSION___or___SET__RIGHT_OPERATOR()

            if attribute(middle_2, name__is_LANGUAGE__SET__RIGHT_OPERATOR):
                return conjure_LANGUAGE_SET_EXPRESSION_1(
                           left_operator__set,
                           middle_1,
                           conjure_LANGUAGE__comma__SET__RIGHT_OPERATOR(operator_1, middle_2),
                       )

            #
            #   many
            #
            return parse_LANGUAGE__SET_EXPRESSION__many(
                    left_operator__set,
                    [middle_1, middle_2],
                    [operator_1],
                )


        #
        #   NOTE:
        #       There is no `name__is_LANGUAGE__optional_comma__RIGHT_OPERATOR`, hence
        #       `name__is_LANGUAGE__SET__RIGHT_OPERATOR` is used for `name__is_LANGUAGE__optional_comma__RIGHT_OPERATOR`.
        #
        parse_LANGUAGE__group_SET_EXPRESSION__many = produce_parse_LANGUAGE__bookcase_expression(
                'TREMOLITE',                                        #   language
                arrange('group_%s', name),                          #   name
                0,                                                  #   LEFT_OPERATOR
                0,                                                  #   conjure_LANGUAGE_bookcase_expression_1
                0,                                                  #   conjure_LANGUAGE_bookcase_expression_2
                0,                                                  #   conjure_LANGUAGE_bookcase_expression_comma_1
                conjure_LANGUAGE_group_SET_EXPRESSION_MANY,         #   conjure_LANGUAGE_bookcase_expression_many
                conjure_LANGUAGE__comma__SET__RIGHT_OPERATOR,       #   conjure_LANGUAGE_dual_token
                0,                                                  #   conjure_LANGUAGE_EMPTY_PAIR
                0,                                                  #   name__is_LANGUAGE__comma__RIGHT_OPERATOR
                name__is_LANGUAGE__SET__RIGHT_OPERATOR,             #   name__is_LANGUAGE__optional_comma__RIGHT_OPERATOR
                name__is_LANGUAGE__SET__RIGHT_OPERATOR,             #   name__is_LANGUAGE_RIGHT_OPERATOR
                0,                                                  #   parse_LANGUAGE__FIRST_expression

                #
                #   parse_LANGUAGE__FIRST_expression__or__RIGHT_OPERATOR
                #
                0,

                #
                #   parse_LANGUAGE__MIDDLE_expression__or__RIGHT_OPERATOR
                #
                parse_LANGUAGE___middle_SET_EXPRESSION___or___SET__RIGHT_OPERATOR,
            )

        parse_LANGUAGE__SET_EXPRESSION__many = produce_parse_LANGUAGE__bookcase_expression(
                'TREMOLITE',                                        #   language
                name,                                               #   name
                0,                                                  #   LEFT_OPERATOR
                0,                                                  #   conjure_LANGUAGE_bookcase_expression_1
                0,                                                  #   conjure_LANGUAGE_bookcase_expression_2
                0,                                                  #   conjure_LANGUAGE_bookcase_expression_comma_1
                conjure_LANGUAGE_SET_EXPRESSION_MANY,               #   conjure_LANGUAGE_bookcase_expression_many
                conjure_LANGUAGE__comma__SET__RIGHT_OPERATOR,       #   conjure_LANGUAGE_dual_token
                0,                                                  #   conjure_LANGUAGE_EMPTY_PAIR
                0,                                                  #   name__is_LANGUAGE__comma__RIGHT_OPERATOR
                name__is_LANGUAGE__SET__RIGHT_OPERATOR,             #   name__is_LANGUAGE__optional_comma__RIGHT_OPERATOR
                name__is_LANGUAGE__SET__RIGHT_OPERATOR,             #   name__is_LANGUAGE_RIGHT_OPERATOR
                0,                                                  #   parse_LANGUAGE__FIRST_expression

                #
                #   parse_LANGUAGE__FIRST_expression__or__RIGHT_OPERATOR
                #
                0,

                #
                #   parse_LANGUAGE__MIDDLE_expression__or__RIGHT_OPERATOR
                #
                parse_LANGUAGE___middle_SET_EXPRESSION___or___SET__RIGHT_OPERATOR,
            )


        return parse_LANGUAGE___SET_EXPRESSION___LEFT_OPERATOR__SET


    parse_TREMOLITE___complement_set_expression___left_brace__tilde = (
            produce_parse_LANGUAGE___SET_EXPRESSION___LEFT_OPERATOR__SET(
                'TREMOLITE',                                        #   langauge
                'complement_set_expression',                        #   name
                'left_brace__tilde',                                #   LEFT_OPERATOR__SET
                'tilde__right_brace',                               #   SET__RIGHT_OPERATOR
                conjure_TREMOLITE__comma__complement__right_brace,  #   conjure_LANGUAGE__comma__SET__RIGHT_OPERATOR
                conjure_TREMOLITE_group_complement_set_expression_1,#   conjure_LANGUAGE_group_SET_EXPRESSION_1

                #
                #   conjure_LANGUAGE_group_SET_EXPRESSION_MANY
                #
                conjure_TREMOLITE_group_complement_set_expression_many,

                conjure_TREMOLITE_complement_set_expression_1,      #   conjure_LANGUAGE_SET_EXPRESSION_1
                conjure_TREMOLITE_complement_set_expression_many,   #   conjure_LANGUAGE_SET_EXPRESSION_MANY
                'is_TREMOLITE__tilde__right_brace',                 #   name__is_LANGUAGE__SET__RIGHT_OPERATOR

                #
                #   name__is_LANGUAGE___simple_atom___or___SET__RIGHT_OPERATOR
                #
                'is_TREMOLITE___simple_atom___or___tilde__right_brace',
            )
        )


    parse_TREMOLITE___set_expression___left_brace__set = (
            produce_parse_LANGUAGE___SET_EXPRESSION___LEFT_OPERATOR__SET(
                'TREMOLITE',                                        #   langauge
                'set_expression',                                   #   name
                'left_brace__set',                                  #   LEFT_OPERATOR__SET
                'set__right_brace',                                 #   SET__RIGHT_OPERATOR
                conjure_TREMOLITE__comma__set__right_brace,         #   conjure_LANGUAGE__comma__SET__RIGHT_OPERATOR
                conjure_TREMOLITE_group_set_expression_1,           #   conjure_LANGUAGE_group_SET_EXPRESSION_1
                conjure_TREMOLITE_group_set_expression_many,        #   conjure_LANGUAGE_group_SET_EXPRESSION_MANY
                conjure_TREMOLITE_set_expression_1,                 #   conjure_LANGUAGE_SET_EXPRESSION_1
                conjure_TREMOLITE_set_expression_many,              #   conjure_LANGUAGE_SET_EXPRESSION_MANY
                'is_TREMOLITE__set__right_brace',                   #   name__is_LANGUAGE__SET__RIGHT_OPERATOR

                #
                #   name__is_LANGUAGE___simple_atom___or___SET__RIGHT_OPERATOR
                #
                'is_TREMOLITE___simple_atom___or___set__right_brace',
            )
        )


    share(
        'parse_TREMOLITE___complement_set_expression___left_brace__tilde',
            parse_TREMOLITE___complement_set_expression___left_brace__tilde,

        'parse_TREMOLITE___set_expression___left_brace__set',
            parse_TREMOLITE___set_expression___left_brace__set
    )
