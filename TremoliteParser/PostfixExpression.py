#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('TremoliteParser.PostfixExpression')
def module():
    class Tremolite_Postfix_NotWordBoundary_Expression(PostfixExpression):
        __slots__    = (())
        display_name = '!/'
        frill        = NOT_WORD_BOUNDARY


        dump_token = dump_token__a__frill__braces


    class Tremolite_Postfix_WordBoundary_Expression(PostfixExpression):
        __slots__    = (())
        display_name = '/'
        frill        = SOLIDUS


        dump_token = dump_token__a__frill__braces


    #
    #   .conjure_postfix_expression
    #
    Tremolite_Operator_NotWordBoundary.conjure_postfix_expression = static_method(
            produce_conjure_postfix_expression(
                'tremolite-postfix-not-word-boundary-expression',
                Tremolite_Postfix_NotWordBoundary_Expression,
            )
        )


    OperatorSolidus.conjure_postfix_expression = static_method(
            produce_conjure_postfix_expression(
                'tremolite-postfix-word-boundary-expression',
                Tremolite_Postfix_WordBoundary_Expression,
            )
        )
