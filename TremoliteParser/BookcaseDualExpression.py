#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('TremoliteParser.BookcaseDualExpression')
def module():
    class Tremolite_Group_BraceExpression(BookcaseDualTwig):
        __slots__    = (())
        display_name = '{group:}'
        frill        = conjure_vwx_frill(LEFT_BRACE__W, W__COLON__W, W__RIGHT_BRACE)

        #<atom>
        is_CRYSTAL_atom = true
        #</atom>


    class Tremolite_Group_ComplementSetExpression_1(BookcaseDualTwig):
        __slots__    = (())
        display_name = '{~group:1~}'
        frill        = conjure_vwx_frill(LEFT_BRACE__TILDE__W, W__COLON__W, W__TILDE__RIGHT_BRACE)

        #<atom>
        is_CRYSTAL_atom = true
        #</atom>


    class Tremolite_Group_OptionalExpression(BookcaseDualTwig):
        __slots__    = (())
        display_name = '[group:]'
        frill        = conjure_vwx_frill(LEFT_SQUARE_BRACKET__W, W__COLON__W, W__RIGHT_SQUARE_BRACKET)

        #<atom>
        is_CRYSTAL_atom = true
        #</atom>


    class Tremolite_Group_SetExpression_1(BookcaseDualTwig):
        __slots__    = (())
        display_name = '{|group:1|}'
        frill        = conjure_vwx_frill(LEFT_BRACE__SET__W, W__COLON__W, W__SET__RIGHT_BRACE)

        #<atom>
        is_CRYSTAL_atom = true
        #</atom>


    #
    #   NOTE:
    #       `conjure_TREMOLITE_*__with_frill` is not used yet, but will be in the future, so left alone for now.
    #
    [
        conjure_TREMOLITE_group_brace_expression, conjure_TREMOLITE_group_brace_expression__with_frill,
    ] = produce_conjure_bookcase_dual_twig(
            'tremolite-group-brace-expresssion',
            Tremolite_Group_BraceExpression,
        )

    [
        conjure_TREMOLITE_group_complement_set_expression_1,
        conjure_TREMOLITE_group_complement_set_expression_1__with_frill,
    ] = produce_conjure_bookcase_dual_twig(
            'tremolite-group-complement-set-expresssion-1',
            Tremolite_Group_ComplementSetExpression_1,
        )

    [
        conjure_TREMOLITE_group_optional_expression, conjure_TREMOLITE_group_optional_expression__with_frill,
    ] = produce_conjure_bookcase_dual_twig(
            'tremolite-group-optional-expresssion',
            Tremolite_Group_OptionalExpression,
        )

    [
        conjure_TREMOLITE_group_set_expression_1, conjure_TREMOLITE_group_set_expression_1__with_frill,
    ] = produce_conjure_bookcase_dual_twig('tremolite-group-set-expresssion-1', Tremolite_Group_SetExpression_1)


    share(
        'conjure_TREMOLITE_group_brace_expression',             conjure_TREMOLITE_group_brace_expression,
        'conjure_TREMOLITE_group_complement_set_expression_1',  conjure_TREMOLITE_group_complement_set_expression_1,
        'conjure_TREMOLITE_group_optional_expression',          conjure_TREMOLITE_group_optional_expression,
        'conjure_TREMOLITE_group_set_expression_1',             conjure_TREMOLITE_group_set_expression_1,
    )
