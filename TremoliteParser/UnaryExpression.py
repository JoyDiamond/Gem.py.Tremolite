#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('TremoliteParser.UnaryExpression')
def module():
    class Tremolite_Unary_NotWordBoundary_Expression(UnaryExpression):
        __slots__    = (())
        display_name = '/'
        frill        = NOT_WORD_BOUNDARY


        dump_token = dump_token__frill__a__braces


    class Tremolite_Unary_NotWordBoundary_Expression(UnaryExpression):
        __slots__    = (())
        display_name = '/'
        frill        = SOLIDUS


        dump_token = dump_token__frill__a__braces


    class TremoliteGroupAtExpression(UnaryExpression):
        __slots__    = (())
        display_name = '@'
        frill        = AT_SIGN


    OperatorSolidus.conjure_unary_expression = static_method(
            produce_conjure_unary_expression(
                'tremolite-unary-word-boundary-expression',
                Tremolite_Unary_NotWordBoundary_Expression,
            )
        )


    Tremolite_Operator_NotWordBoundary.conjure_unary_expression = static_method(
            produce_conjure_unary_expression(
                'tremolite-unary-not-word-bounardy-expression',
                Tremolite_Unary_NotWordBoundary_Expression,
            )
        )

    conjure_TREMOLITE_group_at_expression = produce_conjure_unary_expression(
            'tremolite-group-at-expression',
            TremoliteGroupAtExpression,
        )


    share(
        'conjure_TREMOLITE_group_at_expression',        conjure_TREMOLITE_group_at_expression,
    )
